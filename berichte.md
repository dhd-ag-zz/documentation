[*zurück zur Startseite*](README.md)

# Berichte und Materialien für den DHd-Verband

06/2021

- Metadaten-Workshop: Herbst 2020, vDHd 2021
- Blogbeiträge
- User Stories für Text+
- neue Mitglieder
- Radihum-Interviews zur AG und zum Metadaten-Workshop
- Diskussionspapier: Digitalisierungsrichtlinien der DFG
- AG-Treffen mit Projektvorstellungen
- Aktivitäten auf externen Veranstaltungen

Seit dem letzten Bericht auf der DHd2020 in Präsenz in Paderborn hat die AG-Arbeit ausschließlich virtuell statt gefunden. Neben den regelmäßigen Videokonferenzen in der großen AG-Runde, hat sich durch die Planung einer methodenorientierten Workshop-Reihe kleinere Task Force zur Planung des Metadaten-Analyse-Workshops im Frühjahr 2020 zur Vorbereitung gefunden. Gemeinsam mit den AG-internen Referenten Mark Hall und Andreas Lüschow wurde das Konzept erarbeitet und die Veranstaltung für den Herbst geplant. Gleichzeitig haben sich mehrere Autoren aus der AG gefunden, die dem "Call for User Stories" des NFDI-Konsortiums Text+ gefolgt sind. Während des Sommers sind folgende Stories entstanden und zu den verschiedenen Datendomänen von Text + veröffentlicht worden: https://www.text-plus.org/en/research-data/user-story-403/; https://www.text-plus.org/en/research-data/user-story-309/. Ziel dieser Arbeit war es, die Bedarfe aus der AG in den NFDI-Prozess zu integrieren.

Im September wurde nach intensiver Planung der Workshop zur Analyse von Zeitungs- und Zeitschriftenmetadaten durchgeführt. Die Nachfrage nach den limitierten Teilnahmeplätzen war groß und wir konnten nicht alle Anfragen annehmen. Das Publikum setzte sich aus Forschenden und potentiellen Metadatenanbietenden zusammen. Wichtig war auch hier, dass ein Gespräch zwischen Zeitungs- und Zeitschriftenforschung, die sich nicht in den DH verorten sowie Infrastrukturanbietern, die ihre Datenangebote an der Community ausrichten wollen und der AG in Gang kam. Im Nachgang dieses - und auch des Folgeworkshops - kam es zu zahlreichen Beitritten in die AG. Im Nachgang des Workshops haben wir einen Beitrag für den dhd-Blog veröffentlicht (https://dhd-blog.org/?p=14457) und das große Interesse zum Anlass genommen, den Workshop in leicht abgewandelter Form für die vDHd2021 einzureichen.

Wir sind außerdem gern der Anfrage von Radihum nachgekommen und haben ein Podcast-Interview für die Reihe Digital Humanities AGs mit Radihum aufgenommen: https://radihum20.de/ag-zeitungen-und-zeitschriften/ - auch in der Reihe zur vDHd2021 von Radihum haben wir uns mit einem Beitrag zum Metadaten-Workshop beteiligt: https://radihum20.de/metadaten-workshop/ . Auch beim eigentlichen Workshoptermin in der ersten Event-Woche der vDHd im März 2021 hatten wir wieder ein gemischtes Publikum aus dem Feld der Forschung und Infrastruktur, das sich gleichermaßen für das Thema interessierte. Auch hier wurden neue AG-Mitglieder gewonnen und ein Blogbeitrag für den vDHd-Blog erarbeitet, siehe https://vdhd2021.hypotheses.org/1396 .

Sehr kurzfristig wurde in diesem Frühjahr auch eine Diskussionvorlage für den von der DFG angestoßenen Prozess zur Erneuerung der Praxisregeln Digitalisierung erarbeitet und rechtzeitig zum entsprechenden Rundgespräch im April 2021 an die DFG versendet. Hier geht es uns in Zusammenarbeit mit der eher materialphilologisch orientierten Forschergruppe Journalliteratur darum, die Materialität der Zeitungen und Zeitschriften im Digitalisierungsprozess besser abzubilden.

Neben diesen singulären Aktivitäten haben Mitglieder der AG auf verschiedenen virtuellen Fachtagungen teilgenommen und wir haben die inhaltliche Arbeit in den AG-Treffen verstärkt, indem wir dort kurze Projektvorstellungen eingerichtet haben.

Die AG hatte sich schnell an die pandemiebedingte Arbeit im virtuellen Raum eingestellt, musste aber wie alle anderen auch, starke Einschränkungen hinnehmen, was das Miteinander angeht und auch die Integration neuer Mitglieder, die noch keine Gelegenheit hatten, bisherige AG-Mitglieder persönlich kennen zu lernen. Es geht dabei viel Dynamik verloren, während die Vorteile des ortsunabhängigen Arbeitens auch vor der Pandemie selbstverständlich genutzt wurden.
