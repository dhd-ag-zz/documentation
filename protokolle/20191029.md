[*zurück zur Protokollliste*](../protokolle.md)

# 2019-10-29 Telko

(Teilnehmer*innen: Andrea, Clemens, Ina + Anna, Lisa, Michael, Patrick, Nanette ...)

- Organisatorisches zum geplanten OCR-Workshop am 11.11.
  - Dazu bitte prüfen ob ihr bereits in der Teilnehmerliste eingetragen seid: https://docs.google.com/document/d/1eLa88FHld_Dtd2NHH-8RMEBCjnKpL_D5U6n4iMZ7Wv0/edit
  - Hier das Programm: https://dhd-ag-zz.github.io/workshops/ocr_2019-11-11
  - Das Catering für den Mittagsimbiss sowie Kaffeepause wird dankenswerterweise von HeBIS zur Verfügung gestellt (\@Andrea: Danke für die Einladung :-)
  - Offene Frage der Ergebnisveröffentlichung: Live-Notes: Michael Dahnke, Nanette
    - Am Ende des Workshops: Entscheidung über Veröffentlichung eines Leitfadens: OCR für Zeitungen & Zeitschriften, der die Erfahrungen aus den präsentierten Projekten zusammenfasst und für unterschiedliche Zielgruppen Empfehlungen ausspricht → hier Input der Referent*innen!
    - Deadline bis Jahresende setzen
    - Veröffentlichungsform: Homepage der AG (entsprechend bewerben) und ggf. je nach Ergebnis weitere Veröffentlichung als Paper in Zeitschrift o.ä.
  - Für die Vorstellungsrunde: Aufforderung vorab an alle Teilnehmenden ihren Gegenstand beispielhaft in einem Satz zu beschreiben und wenn möglich Beispielseite mitzubringen. (z.B. Ich arbeite mit Zeitschriften aus dem 20. Jahrhundert, aus dem romanischen Sprachraum. Die Zeitschriften liegen als Bilddokumente vor und sind mit bibliografischen Metadaten im Dublin Core/ Mets/Mods etc. Format versehen. Mein Forschungsinteresse ist …)
  - Es wurde angeregt an Namensschilder (gern auch zum selbst schreiben) zu denken (Danke an \@Andrea)
- Weitere Planungen: Nächster Workshop "Methodenworkshop: Annotation - Transformation - Interpretation" (Vorschlag von Torsten, Nanette)
  - Jährliche Workshops wären generell anzustreben (?)
  - Zum Methodenworkshop wäre die Frage, ob es Menschen in der AG gibt, die bereits einen Workflow z.B. zur Analyse (oder auch nur zur Materialvorbereitung) haben und diesen anderen nicht nur vorstellen, sondern auch zur Anwendung auf eigenes Material vermitteln würden? Die Beantwortung der Frage, inwieweit sind dies auf verschiedene Materialien übertragen lässt, würde auch durch die Betrachtung der Gegenstände in der Vorstellungsrunde des OCR-Workshops (oder auch in der Sammlung aus unserem ersten Treffen: https://docs.google.com/document/d/1-UUKgYGbfb7PPS6hAVpUYcFpH7qceGzHY3q4K7beNT4/edit) vereinfacht.
- Sonstiges
  - Es wurde erneut daran erinnert, dass wir uns vorgenommen hatten, uns gegenseitige über Einreichungen bei Konferenzen (z.B. DHd2020 etc.) bzgl. Vorträgen, Panels, etc. die mit Zeitungen & Zeitschriften zu tun haben, informieren. Die digitale Bibliothek der DNB (Lisa Landes, Patrick Dinger) hat ihre Ankündigung bereits per mailing-list geteilt. Siehe auch Punkt 3 von letzter Telko (28.08.19) unten...
  - Bei der TEI-Konferenz in Graz (Sept. 2019) waren Dario, Torsten, Patrick zum Treffen der SIG Periodicals anwesend. Dort wird an einer allgemeinen Struktur für tags von Zeitungen & Zeitschriften überlegt. Hier das Protokoll: https://docs.google.com/document/d/11iw5IY64zbqubSGgStBz_6LGmR24Kh41pxDlCMYS-jY/edit?ts=5d80cdd2#heading=h.fc5yc3g0fsin
- Nächster Telko-Termin?
  - Eigentlich war der 16.12.19 vorgesehen, aber dies werden wir vermutlich absagen, weil zu diesem Zeitpunkt eher Arbeit in kleineren Gruppen zur Finalisierung der Ergebnisveröffentlichung des OCR-Workshops angesagt ist. Zum Folgetermin im Januar gibt es eine Terminumfrage: aktuell favorisierte Zeit ist Dienstags 17 Uhr. Gegenstimmen gerne melden.
