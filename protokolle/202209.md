[*zurück zur Protokollliste*](../protokolle.md)

# 2022-09 Telko

Meeting RAUM: https://meet.gwdg.de/b/nan-upd-fgl-nad

Alter Agendaentwurf vom Juni:

- Wer ist heute da?
- Begrüßung neuer Mitglieder
- News/ Berichte?
- Barcamp DHd2022
  - Follow-Up?
- Stand: Task Force Zotero-Library
- Workshops
  - Blogpost: Matthias und Harald zum Korpusworkshop
    - bitte um Rückmeldung
  - Wiederholung des Korpusworkshops
- Öffentlichkeitsarbeit
  - noch ein Hero Image für Homepage basteln
  - Aufforderung per Mail schicken
  - AG-Twitter-Hashtag: #dhdagzz
    - Überarbeitung Best-Practices-Seite: Team: Torsten, Nanette, Tinghui
    - Umfrage: https://forms.gle/QwnzeW74H8Z7kJ5k6
    - Diskussion, Änderungsvorschläge, Auswertung der Rückmeldungen
- Produktives
  - offener Vorschlag: RIDE-Rezensionenband zu Zeitungsportalen (siehe https://ride.i-d-e.de/)
  - weiteres?
- Projekt-Vorstellung??
  - Bitte melden!
- Sonstiges
  - Torsten: Übung "Zeitungen & Zeitschriften" => ggf. näherer Austausch über Z&Z in der Lehre?
  - Thema: Listen in Zeitungen / Zeitschriften (Hochzeitssachen, Todesanzeichen, etc)
