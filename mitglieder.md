[*zurück zur Startseite*](README.md)

(Diese Seite dient dazu, die Kontaktdaten der Mitglieder der AG festzuhalten. Mehr Informationen zu den einzelnen Mitgliedern s. [Interessen-Seite](members/interests.md))

| Name | Institution | Email | Skype |
|---|---|---|---|
| Mark Hall | The Open University | mark.hall@open.ac.uk | archchancellor_ridcully |
| Nanette Rißler-Pipka | GWDG | nanette.rissler-pipka@gwdg.de | narissler |
| Torsten Roeder | Uni Wuppertal | dh@torstenroeder.de | tr-206 |
| Ina Serif | Uni Basel | ina.serif@unibas.ch | ina-aus-china |
| Patrick Dinger | ULB Münster | patrick.dinger@uni-muenster.de | svenson892 |
| Anja Piller | ULB Sachsen-Anhalt | anja.piller@bibliothek.uni-halle.de | apiller1 |
| Hanno Ehrlicher | Uni Tübingen |  | hanno.el.cuco |
| Lisa Landes | Deutsche Nationalbibliothek | l.landes@dnb.de | Lisa Landes |
| Matthias Arnold | Uni Heidelberg | matthias.arnold@uni-hd.de | matz-skype |
| Claudia Resch | Österr. Akademie der Wissenschaften | claudia.resch@oeaw.ac.at | claudia.resch16 |
| Lino Wehrheim | Uni Regensburg | lino.wehrheim@ur.de | lino_wehrheim |
| Harald Lordick | Steinheim-Institut (an der UniDUE) | lor@steinheim-institut.org | harald.lordic |
| Angela Vorndran | Deutsche Nationalbibliothek | a.vorndran@dnb.de | avorndran |
| Marjam Trautmann | Akademie d. Wissenschaften und Literatur Mainz (Digitale Akademie) | marjam.trautmann@adwmainz.de | marjam.m. |
| Christian Sieg | WWU Münster | christian.sieg@wwu.de | Christian Sieg |
| Tinghui Duan | Uni Trier | duan@uni-trier.de |  |
| Hendrikje Schauer | Europa-Universität Viadrina | schauer@europa-uni.de |  |
| Vincent Fröhlich | Uni Marburg | vincent.froehlich@staff.uni-marburg.de | tubbsucrockett |
| Urte Helduser | Carl von Ossietzky Universität Oldenburg | urte.helduser@uni-oldenburg.de | uhelduser |
| Martin Dröge | Humboldt-Universität zu Berlin | martin.droege@hu-berlin.de |  |
| Clemens Neudecker | Staatsbibliothek zu Berlin | clemens.neudecker@sbb.spk-berlin.de |  |
| Stefanie Läpke | ULB Bonn | slaepke@uni-bonn.de |  |
| Sarah Oberbichler | Uni Innsbruck | sarah.oberbichler@uibk.ac.at |  |
| Eva Pfanzelter | Uni Innsbruck | eva.pfanzelter@uibk.ac.at |  |
| Jana Keck | German Historical Institute Washington | keck@ghi-dc.org |  |
| Haimo Stiemer | Carl von Ossietzky Universität Oldenburg | haimo.stiemer@uol.de |  |
| Christian Wachter | Georg-August-Universität Göttingen | christian.wachter@uni-goettingen.de |  |

Mailinglist: http://lists.lists.digitalhumanities.org/mailman/listinfo/dhd-ag-zz
