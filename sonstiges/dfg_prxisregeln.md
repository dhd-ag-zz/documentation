[zurück zur Startseite](../README.md)

Anlass: Bedarf aus Zusammenarbeit mit Forschergruppe Journalliteratur: Größe, Format. Materialeigenschaften des Originals werden bei Digitalisierungsprozess nicht als Metadaten erfasst

Termin: 26.04. trifft sich das Gremium zur Überarbeitung der DFG Praxisregeln Digitalisierung

Action: Brief an die DFG im Namen der AG und ggf. auch im Namen der Forschergruppe Journalliteratur mit der Bitte um Besprechung des Bedarfs/Desiderats und Rückmeldung

Die AG Zeitungen & Zeitschriften des Dachverbandes "Digital Humanities im deutschprachigen Raum" existiert seit 2019. Sie fördert den Austausch zwischen der breiten und interdisziplinären Gruppe der Zeitungs- und Zeitschriftenforschung und den Infrastruktureinrichtungen, die digitale Periodika anbieten. In den letzten Jahren konzentrierte sie sich auf die Vermittlung von Kompetenzen (OCR, Metadaten), Netzwerkbildung in die diversen wissenschaftlichen Zweige hinein sowie den Austausch über Technologien und Forschungsfragen. Außerdem fungiert die AG als Sprachrohr der Community im NFDI-Prozess und ist Ansprechgruppe für Fragen zur zeitungs- und zeitschriftenbasierten Forschung. Für den anstehenden Prozess der community-orientierten Erneuerung der DFG-Praxisregeln "Digitalisierung" sind wir gerne bereit, als Vermittler aufzutreten und aktiv an der Gestaltung mitzuwirken.

Beiliegend finden Sie in diesem Sinne einen ersten Impuls für zukünftige Diskussionen.

# Brief-Entwurf – zur Diskussion beim AG-Treffen: 22.04.

Die Formatgröße (aber auch weitere Angaben zur Materialität des Originals), die korrekte Anzeige des visuellen Auftritts der Zeitung/Zeitschrift und die Vollständigkeit der Ausgabe sind insbesondere für die Forschung an historischen Zeitungen und Zeitschriften von besonderem Interesse. Auch wenn der Begriff des "Originals" bei Zeitungen und Zeitschriften, die in Bibliotheken und Archiven oft zu Bänden zusammengebunden aufbewahrt und notwendig beschnitten werden, kritisch hinterfragt werden muss, sind bei historischen Druckerzeugnissen Angaben zur Materialität, Vollständigkeit, Anzeige und Format wichtige Indikatoren im Rezeptionsprozess.

Im Zuge der erfreulichen Massendigitalisierung sind wir weniger darauf angewiesen, das Original selbst zu konsultieren, das im Falle von Zeitungen und Zeitschriften oft auf wenig langlebigen Material gedruckt ist. Das Digitalisat bleibt zunehmend die einzige Überlieferung dieser Zeitzeugen auf Papier. Auch die quantitativen Untersuchungen durch digitale Methoden, die angesichts der zahlreichen Titel, Ausgaben und Seiten die Analyse neuer Bereiche innerhalb der Zeitungs- und Zeitschriftenforschung erst ermöglichen, sind auf die Digitalisate und damit erzeugten Meta(Daten) angewiesen. Informationen zu Vollständigkeit, Größe, Format, Material sind in diesem Sinne Basisinformationen, die für eine größere Akzeptanz und Nutzung der Digitalisate innerhalb der Forschung sorgen.

In drei wesentlichen Punkten möchten wir vorschlagen, dass die Praxisregeln Digitalisierung den Bedürfnissen der Community der Forschenden zu Zeitungen und Zeitschriften entgegenkommen und hier unter Umständen gemeinsam neue Standards erarbeitet werden:

1. Angabe von Format und Größe
1. Angabe zur Vollständigkeit der Ausgabe
1. Korrekte Darstellung des doppelseitigen Auftritts der jeweiligen Zeitschrift / Zeitung

## 1. Angabe von Format und Größe

Es gibt zahlreiche Beispiele für Zeitschriften, insbesondere zu Beginn des 20. Jahrhunderts, die in Format stark abweichen. Extreme Fälle sind Prisma hg. von José Luis Borges, die als berühmte „Mauerzeitschrift“ im Plakatformat erscheint und an Littfasssäulen veröffentlicht wird. Ohne dieses Kontextwissen, gibt das Digitalisat allein diese Information jedoch nicht preis: https://ahira.com.ar/revistas/prisma/ (abgesehen davon, dass hier keine Metadaten angegeben werden). Es ließen sich zahlreiche weitere Beispiele auch aus dem deutschsprachigen Raum und in digizeitschriften finden: hier die bibliographischen Metadaden der Zeitschrift Pan: https://katalog.ub.uni-heidelberg.de/cgi-bin/titel.cgi?katkey=66629903&sess=cbbb8c2e551fcd38c0288748583dbfff&query=si%3A558999948%20-%28fac_teil%3Aezblf%29&sort=0&format=html. Bei neueren Digitalisaten bieten einige Bibliotheken seit wenigen Jahren den Kompromiss einer Farb- und Maßstabskale als letzte Seite des Digitalisats an: https://gdz.sub.uni-goettingen.de/id/PPN1690533838?tify={%22pages%22:[213,%22view%22:%22info%22]} oder https://digi.ub.uni-heidelberg.de/diglit/form1922/0347. Dies ermöglicht die einfache und menschenlesbare Ableitung der Größenverhältnisse. Die Information selbst ist jedoch recht versteckt (im Inhaltsverzeichnis) und nicht in den Metadaten eingetragen und damit für quantitative Analysen nicht automatisiert, maschinenlesbar abrufbar.

Es kann von Nutzenden, die meist leider ohnehin darauf hin „erzogen“ werden, mit einem Viewer online zu blättern, nicht erwartet werden, dass sie die Größe mit „Photoshop“ o.ä. über das Verhältnis der Pixeldimensionen (x, y) in Abhängigkeit zur Auflösung die Größen errechnen. Bei diesem Verfahren liegen zudem weitere Fehlerquellen auf der Hand, die das Ergebnis und damit die Datengrundlage verfälschen können.

Zwei Möglichkeiten zur Lösung des Desiderats, die am besten kombiniert werden sollten, schlagen wir vor:

- Es könnten entsprechende Angaben zur Größe gemacht werden: <physicalDescription> /<extent>. Die „measurement"-Daten könnten in den METS containern bzw. auch auf der MODS ebene miterfasst werden. Jedoch wird das Original im Digitalisierungsprozess in den meisten Fällen nicht vermessen und keine weiteren Materialeigenschaften erfasst. Das Feld bleibt demnach leer - sicher auch eine notwenige Nutzen-Kosten-Abwägung im Digitalisierungsprozess, aber in jedem Fall ein Desiderat für die Forschung.
- Zudem wäre es natürlich wünschenswert, wenn die Daten direkt auf der Bild-Ebene hinterlegt werden, zum Beispiel für den Einzelbild-Export, idealerweise innerhalb des Bild-Containers (zb TIF) unter Ausnutzung der sog. embedded metadata. Diese können entweder direkt in die bilddatei metadaten (zb EXIF) oder die adobe xmp sidecar files (xmp, cf. XMP Specification Public Patent License) eingegeben werden.

Zumindest die grundlegenden Maße, wie sie für jedes digitale Objekt im Kontext von Museen selbstverständlich notiert werden, sollten einsehbar sein Das Deutsche Historische Museum Berlin gibt in der Digitalen Bibliothek diese Maße auch für Zeitungen/Zeitschriften und andere Druckwerke: https://www.deutsche-digitale-bibliothek.de/item/H437AGSXILG4OFGLHYSF7S2CBM4W2GGP

## 2. Angabe zur Vollständigkeit der Ausgabe

Bei der Arbeit mit einer größeren Menge an Zeitschriften-Ausgaben fällt zudem auf, dass die digitalisierten Ausgaben häufig unvollständig sind, darauf aber nicht hingewiesen wird. Die Unvollständigkeit betrifft sehr häufig den Annonceteil und den Umschlag, darüber hinaus aber auch einzelne Seiten innerhalb der Ausgabe. In den Recherchen über Fontanes Stechlin in Über Land und Meer in fünf unterschiedlichen privaten, im Fontane Archiv vorhandenen sowie digitalisierten Exemplaren des entsprechenden Halbjahrgangsbandes variiert die Anzahl der Seiten des Annoncenteils jedes Mal. Sollte eine Seite der Zeitschriftenausgabe fehlen, müsste eine Notiz zu Anfang der entsprechenden Nummer darauf aufmerksam machen. So wird verhindert, dass Forscher:innen Ausgaben auswerten und erst später im Vergleich mit dem Original feststellen, dass ihre Daten unvollständig sind. Zusätzlich sollte an der entsprechenden Stelle in der jeweiligen Ausgabe eine Leerseite mit Hinweis über die fehlende Seite eingefügt sein, damit die Struktur der Zeitschriftenausgabe, digitale Durchblätterbarkeit und der doppelseitige Auftritt der Ausgabe möglichst gut nachvollziehbar sind.

## 3. Korrekte Darstellung des doppelseitigen Auftritts der jeweiligen Zeitschrift / Zeitung

Um den Eindruck der Zeitschrift auf die Leser:innen sowie semantische und serielle Beziehungen auf der Doppelseite nachvollziehen zu können wären Digitalisate wünschenswert, die sich besser zu Doppelseiten verbinden lassen. Selbst beim DFG-Viewer und den mit ihm angeführten Beispielen wird die Doppelseitigkeit nicht richtig hergestellt, da die Einzelseiten größtenteils zu viel Überhang haben. Zudem stellt für die Community auch die Nutzbarkeit der Doppelseitigkeit im DFG-Viewer eine Herausforderung dar. Wenn der Viewer ein Dokument mit der Titelseite beginnt, werden häufig gerade jene Seiten optisch verbunden, die eigentlich keine Doppelseiten sind (recto und verso statt verso und recto). Dies ist bspw. bei folgender Beispieldatei des DFG-Viewer der Fall: Ryff, Walther Hermann: Der furnembsten, notwendigsten, der gantzen Architectur angehoerigen Mathematischen und Mechanischen kuenst eygentlicher bericht und vast klare, verstendliche unterrichtung. Nürnberg 1547.

http://dfg-viewer.de/show?tx_dlf%5Bdouble%5D=1&tx_dlf%5Bid%5D=http%3A%2F%2Fdigital.slub-dresden.de%2Foai%2F%3Fverb%3DGetRecord%26metadataPrefix%3Dmets%26identifier%3Doai%3Ade%3Aslub-dresden%3Adb%3Aid-263566811&tx_dlf%5Bpage%5D=5&tx_dlf%5Bpagegrid%5D=0&cHash=7689c87a606ca387b6871b642ae585a1)

Die Korrekturfunktion der doppelseitigen Ansicht, bei der also die richtigen beiden Seiten zu einer Doppelseite verbunden werden, ist den meisten Nutzer:innen nicht bewusst. Als Praxisanleitung könnte in den Digitalisaten und im Viewer expliziter auf diese Funktion hingewiesen werden.

Es wäre ein erster Schritt und eine große Hilfe für die Community der Zeitungs- und Zeitschriftenforschung, wenn die diese drei Punkte in den Überlegungen zu den DFG-Praxisregeln Mitberücksichtigung finden.

Die praktische Umsetzung solcher Community-Anforderungen mag Herausforderungen und Änderungsbedarf für etablierte Digitalisierungworkflows mit sich bringen. Niedrigschwellige, schrittweise Anpassungen könnten bei 'minimalinvasiven' Punkten beginnen, etwa mit der konsequenten Einbettung der Metadaten (einschließlich Dimensionsdaten) in die Seitenimages (Binärdateien) und der Miterfassung einer Farbreferenzkarte (Color Chart) einschließlich Maßstab.
