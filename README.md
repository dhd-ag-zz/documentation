# Dokumente

## Inhaltsverzeichnis

- [Mitglieder](mitglieder.md)
- [Sitzungsprotokolle](protokolle.md)
- [Berichte](berichte.md)
- [Workshops](workshops.md)
- [DHd2022-Potsdam](dhd2022.md)
- [User Stories NFDI Text+]
- [Task Force Bibliographie / Zotero]
- [Öffentliche Arbeit]
- [Input DFG Praxisregeln Digitalisierung]
